from django.shortcuts import render
from django.http import JsonResponse
from django.utils import timezone
# from .models import ImageData, ImageSample
# from .helpers import image_processing as imgps
# from .helpers import machine_learning as ml

def index(request):
  view = {'message': 'Hello World'}
  return render(request, 'main/index.html', view)
